package micdm.wschat;

import micdm.wschat.interfaces.INickKeeper;
import micdm.wschat.websockets.IWsConnectorKeeper;
import micdm.wschat.websockets.WebsocketConnector;

public class Application extends android.app.Application implements INickKeeper, IWsConnectorKeeper {

    protected String nick;
    protected final WebsocketConnector connector = new WebsocketConnector(Config.SERVER_ADDRESS);

    @Override
    public String getNick() {
        return nick;
    }

    @Override
    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public WebsocketConnector getConnector() {
        return connector;
    }
}
