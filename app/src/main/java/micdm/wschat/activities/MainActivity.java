package micdm.wschat.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import micdm.wschat.R;
import micdm.wschat.fragments.ChatFragment;
import micdm.wschat.fragments.SelectNickFragment;
import micdm.wschat.interfaces.INickKeeper;

public class MainActivity extends AppCompatActivity implements INickKeeper {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a__main);
        setupContent(getNick());
    }

    protected void setupContent(String nick) {
        Fragment fragment = (nick == null) ? SelectNickFragment.getInstance() : ChatFragment.getInstance(nick);
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.a__main__content, fragment)
            .commit();
    }

    @Override
    public String getNick() {
        return ((INickKeeper) getApplication()).getNick();
    }

    @Override
    public void setNick(String nick) {
        ((INickKeeper) getApplication()).setNick(nick);
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.a__main__content, ChatFragment.getInstance(nick))
            .commit();
    }
}
