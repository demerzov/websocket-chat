package micdm.wschat.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import micdm.wschat.R;
import micdm.wschat.websockets.IWsConnectorKeeper;
import micdm.wschat.websockets.WebsocketConnector;

public class ChatFragment extends Fragment {

    protected static class MessageAdapter extends BaseAdapter {

        protected final Context context;
        protected final List<String> messages = new ArrayList<>();

        public MessageAdapter(Context context) {
            this.context = context;
        }

        public void addMessage(String message) {
            messages.add(message);
        }

        @Override
        public int getCount() {
            return messages.size();
        }

        @Override
        public String getItem(int position) {
            return messages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.f__chat__item, parent, false);
            }
            ((TextView) view).setText(getItem(position));
            return view;
        }
    }

    protected static final String ARGUMENT_NICK = "nick";

    protected WebsocketConnector wsConnector;

    @Bind(R.id.f__chat__connecting)
    protected View connectingView;
    @Bind(R.id.f__chat__error)
    protected View errorView;
    @Bind(R.id.f__chat__content)
    protected View contentView;
    @Bind(R.id.f__chat__messages)
    protected ListView messagesView;
    @Bind(R.id.f__chat__message)
    protected TextView messageView;

    public static Fragment getInstance(String nick) {
        Bundle args = new Bundle();
        args.putString(ARGUMENT_NICK, nick);
        Fragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        wsConnector = ((IWsConnectorKeeper) getActivity().getApplication()).getConnector();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f__chat, container, false);
        ButterKnife.bind(this, view);
        setStateInitial();
        setupWsConnector();
        return view;
    }

    protected void setupWsConnector() {
        wsConnector.setListener(new WebsocketConnector.IListener() {

            @Override
            public void onInit() {
                wsConnector.connect();
            }

            @Override
            public void onConnecting() {
                setStateConnecting();
            }

            @Override
            public void onConnect() {
                setStateConnected();
            }

            @Override
            public void onNeedGreeting() {
                wsConnector.send(getString(R.string.f__chat__enter_message, getArguments().getString(ARGUMENT_NICK)));
            }

            @Override
            public void onMessage(String message) {
                MessageAdapter adapter = (MessageAdapter) messagesView.getAdapter();
                if (adapter == null) {
                    adapter = new MessageAdapter(getContext());
                    messagesView.setAdapter(adapter);
                }
                adapter.addMessage(message);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onDisconnect() {
                setStateDisconnected();
            }
        });
    }

    @OnClick(R.id.f__chat__reconnect)
    protected void onNeedReconnect() {
        wsConnector.connect();
    }

    @OnEditorAction(R.id.f__chat__message)
    protected boolean onFastSubmitMessage() {
        return submitMessage();
    }

    @OnClick(R.id.f__chat__submit)
    protected void onSubmitMessage() {
        submitMessage();
    }

    protected boolean submitMessage() {
        String message = messageView.getText().toString();
        if (message.length() != 0) {
            wsConnector.send(getString(R.string.f__chat__regular_message, getArguments().getString(ARGUMENT_NICK), message));
            messageView.setText("");
            return true;
        }
        return false;
    }

    protected void setStateInitial() {
        connectingView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        contentView.setVisibility(View.GONE);
    }

    protected void setStateConnecting() {
        connectingView.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        contentView.setVisibility(View.GONE);
    }

    protected void setStateConnected() {
        connectingView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
    }

    protected void setStateDisconnected() {
        connectingView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
        contentView.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        wsConnector.removeListener();
        ButterKnife.unbind(this);
    }
}
