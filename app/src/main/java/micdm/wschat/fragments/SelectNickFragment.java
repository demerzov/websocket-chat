package micdm.wschat.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import micdm.wschat.R;
import micdm.wschat.interfaces.INickKeeper;


public class SelectNickFragment extends Fragment {

    @Bind(R.id.f__select_nick__nick)
    protected TextView nickView;

    public static Fragment getInstance() {
        return new SelectNickFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f__select_nick, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnEditorAction(R.id.f__select_nick__nick)
    protected boolean onFastSubmitNick() {
        return submitNick();
    }

    @OnClick(R.id.f__select_nick__submit)
    protected void onSubmitNick() {
        submitNick();
    }

    protected boolean submitNick() {
        String nick = nickView.getText().toString().trim();
        if (nick.length() != 0) {
            ((INickKeeper) getActivity()).setNick(nick);
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
