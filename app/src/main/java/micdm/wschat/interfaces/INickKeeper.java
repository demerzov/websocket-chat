package micdm.wschat.interfaces;

public interface INickKeeper {

    String getNick();
    void setNick(String nick);
}
