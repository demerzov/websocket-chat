package micdm.wschat.websockets;

public interface IWsConnectorKeeper {

    WebsocketConnector getConnector();
}
