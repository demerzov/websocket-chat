package micdm.wschat.websockets;

import android.os.Handler;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketListener;
import com.neovisionaries.ws.client.WebSocketState;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WebsocketConnector {

    public interface IListener {
        void onInit();
        void onConnecting();
        void onConnect();
        void onNeedGreeting();
        void onMessage(String message);
        void onDisconnect();
    }

    protected final String serverAddress;
    protected WebSocket websocket;
    protected IListener listener;
    protected boolean isGreetingNeeded;
    protected final List<String> messages = new ArrayList<>();

    public WebsocketConnector(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public void setListener(IListener listener) {
        this.listener = listener;
        if (websocket == null || websocket.getState() == WebSocketState.CREATED) {
            listener.onInit();
        }
        if (websocket != null && websocket.getState() == WebSocketState.CONNECTING) {
            listener.onConnecting();
        }
        if (websocket != null && websocket.getState() == WebSocketState.OPEN) {
            listener.onConnect();
            if (isGreetingNeeded) {
                listener.onNeedGreeting();
                isGreetingNeeded = false;
            }
        }
        for (String message: messages) {
            listener.onMessage(message);
        }
        if (websocket != null && websocket.getState() == WebSocketState.CLOSED) {
            listener.onDisconnect();
        }
    }

    public void removeListener() {
        listener = null;
    }

    public void connect() {
        if (websocket != null) {
            websocket.clearListeners();
            websocket.disconnect();
        }
        try {
            websocket = new WebSocketFactory().createSocket(serverAddress);
            websocket.addListener(getWebsocketListener());
            websocket.connectAsynchronously();
        } catch (IOException e) {
            listener.onDisconnect();
        }
    }

    protected WebSocketListener getWebsocketListener() {
        final Handler handler = new Handler();
        return new WebSocketAdapter() {

            @Override
            public void onStateChanged(WebSocket websocket, final WebSocketState state) throws Exception {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (listener == null) {
                            return;
                        }
                        if (state == WebSocketState.CONNECTING) {
                            listener.onConnecting();
                        }
                        if (state == WebSocketState.OPEN) {
                            listener.onConnect();
                        }
                        if (state == WebSocketState.CLOSED) {
                            listener.onDisconnect();
                        }
                    }
                });
            }

            @Override
            public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (listener != null) {
                            listener.onNeedGreeting();
                            isGreetingNeeded = false;
                        } else {
                            isGreetingNeeded = true;
                        }
                    }
                });
            }

            @Override
            public void onTextMessage(WebSocket websocket, final String text) throws Exception {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        String message = getTextMessage(text);
                        if (message == null) {
                            return;
                        }
                        messages.add(message);
                        if (listener != null) {
                            listener.onMessage(message);
                        }
                    }
                });
            }

            @Override
            public void onError(WebSocket websocket, WebSocketException exception) throws Exception {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (listener != null) {
                            listener.onDisconnect();
                        }
                    }
                });
            }
        };
    }

    protected String getTextMessage(String text) {
        if (!text.startsWith("a[")) {
            return null;
        }
        try {
            return new JSONArray(text.substring(1)).getString(0);
        } catch (JSONException e) {
            return null;
        }
    }

    public void send(String message) {
        websocket.sendText(new JSONArray().put(message).toString());
    }
}
